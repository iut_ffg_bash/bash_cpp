Projet Bash
============

##Cahier des charges
_Le cahier des charges permet de bien saisir le sujet et de ne pas partir dans tout les sans._

Premièrement, le programme **doit fonctionner sous Linux** au minimum !

Ensuite on ne travaille **que sur des fichiers text**.

On peut donc vérifier qu’un fichier est un fichier texte avec la commande bash : `file <fichier>`

Les fichiers text **renvoient toujours la valeur _“ASCII text”_**, même les fichiers UTF-8.
Toutes autre valeur ne fait pas référence à un fichier texte.

On doit indexer avec un programme C++ tout les mots contenu dans touts les dossiers de l’arborescence que l’utilisateur à donné dans le console.

###Resultat à obtenir
_L'utilisateur doit pouvoir rentrer les commmandes suivantes :_

* Indexation

`notreScript.bash --index /lenom/durep `

`indexAndSearch.bash --index /home/jdupont`

Pour pouvoir lancer l'indexation des fichiers dans le répertoire spécifié.

* Recherche

`indexAndSearch.bash --search "M1101"`

donc

`notreScript.bash --search "leMotAChercher"` ou bien pour plusieurs mots

`notreScript.bash --search "mot1" "mot2" "mot3"`

Pour pouvoir lancer la recherche d'un **ou pluseurs mots** dans la base de donnée.
Mais déjà il faut faire en sorte que ça marche avec une recherche d'un seul mot.

* Nétoyage

`notreScript.bash --clean`

Pour pouvoir vider les données d'indexation.

* Affichage HTML

`notreScript.bash --html -h`

Pour pouvoir récupérer un fichier au format html **des resultats**.


##Rôle du C++

Le rôle du C++ est d’indexer les fichiers.

C'est à dire de parcourir tout les fichier dans le dossier spécifié par l'utilisateur, ainsi que dans tout ses sous dossiers.

##Rôle du Bash

Le bash interprète les commandes de l’utilisateur, appelle le programme C++ si il y à besoin d’indexer, il recherche dans le fichier qui ont été indexés, crée et envoie le résultat à l’utilisateur.

###Md5sum
Le bash doit gèrer aussi le md5sum.
Il doit verifier que les fichiers n'on pas été modifier depuis la dernière indexation.
Et il doit également,lors de l'inexation, lier la somme md5 au fichiers pour ne pas tous les réindexer.

##Base de donnée

On utilise un fichier "index_global" qui contient tous les mots référencées par l’indexation du programme C++ (en 1 seule fois).
Cela permetra au programme bash de savoir, lors de la recherche, si le mot demander existe ou non beaucoup plus rapidement que de parcourir tout les fichiers

Elle resemblera surement à ça :

![exemple d'organisation de fichiers ](res/exemple_organisation_dossier_a.png)

###Fichier d'indexation global

Ce fichier sera unique et contiendra la liste des mots qui ont été indexé, en une seule fois, pour permètre de verifier rapidement, lors de la recherche, si le mot rechercher existe.

Le fichier d'index global resemblera donc a ça :

![exemple de fichier d'index](res/exemple_fichier_index.png)


###Fichier d'index individuel des mots

Chaque mot aura un fichier qui lui est propre.

Lequel contiendra :

1. un caractère `|`
2. la somme md5
3. le nom du fichier
4. le répertoire du fichier
5. un retour a la ligne
6. Le nombre de fois où le mot apparait dans le fichier
7. puis tout les numéros de ligne espacé d'un "I".

On obtient donc la structure suivante :

```
|<md5sum> <nomDuFichier> <repertoireDuFichier>
<nombreOccurences>
<n°ligneOccurence1>I<n°ligneOccurence2>I<...>I<n°ligneOccurenceN>
```

Par exemple :

![exemple de fichier d'inexation des mots ](res/exemple_fichier_mot.png)

##Normes

* On met un entête au début de nos fichiers pour les repérer et ne pas les indexer.
L'entête est au format :`#index[chaîne] `
où chaîne est une chaîne de caractère assez spécifique pour ne pas être recherchée par l'utilisateur.

* On utilisera des alias pour les commandes :

1. `-i ` pour `--index`
2. `-s` pour `--search`
3. `-c`pour `--clean`
4. `-h`pour `--html`

**Les normes doivent être précisées.**

##Organisation

###Git

On pourra diviser le projet en branches pour travailler sur des fonctionalitées que l'on veut intégrer sans pour autant toucher au master qui est censé marcher.

###Questions à poser

Pourquoi (entre 2 espaces) ? (Voir deuxième point du sujet.)

Est ce qu’on peut changer les format proposé ? (surement, à vérifier)

~~Qui fait la recherche dans la bdd ? C++ ou bash ?~~ (bash voir fin du sujet)

~~Fichier txt uniquement ou fichier, pdf , odt, docx ?~~ (supposé que des fichiers texte, qui renvoient ASCII text avec la commande “file”).