#!/bin/bash
if [ ! -d "$HOME/bin" ]
then
	mkdir $HOME/bin
	export PATH=$PATH:$HOME/bin
	cp script/idnsc /usr/bin
fi
ourdir=$HOME/.indexAndSearch
mkdir $ourdir
mkdir $ourdir/script
ourdir=$HOME/.indexAndSearch/script

for script in ls script
do
	cp script/$script $ourdir
	chmod +x $ourdir/$script
done
