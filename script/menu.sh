case $1 in #On regarde si l'option prise en entré fait partie des commande gérer sinon petit message d'erreur
	"--index" | "-i")
		if [ "$#" -eq "2" ]
		then
			./detecText2.sh `echo $@ | sed 's/'$1'//'`
		else
			echo "PATH not found or too manu argument try --help for more information"
		fi
	;;

	"--search" | "-s")
		if [ "$#" -ne "1" ]
		then
			./search.sh `echo $@ | sed 's/'$1'//'`
		else
			echo "Argument not found try help for more information"
		fi
	;;

	"--html" | "-x")
		echo "sorti html"
	;;
	
	"--help" | "-h")
	
		if [ "$#" -eq "1" ]
		then
			./help.sh
		else
			case $2 in
				"index" | "i" )
					./helpindex.sh
				;;
			
				"search" | "s" )
					./helpsearch.sh
				;;
			
				*)
					echo "$2 not found try --help for more information"
			esac
		fi
	;;

	*)
		echo "Unknown parameter try --help for more information"
esac
