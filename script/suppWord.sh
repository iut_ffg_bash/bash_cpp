#!/bin/bash
#Ce script doit supprimer le fichier qu'elle prend en parametre de la base de données

dir="$HOME/.indexAndSearch/word"
tmp=mktemp
cpt=0

for fic in `ls $dir`
do
	while read line
	do
	
		if [ `echo $line | grep -c "$1:$2"` -eq "1" ]
		then
			cpt=1
		elif [ $cpt -eq 1 ]
		then
			cpt=0
		else
			echo $line>>$tmp
		fi
		
	done < "$dir/$fic"
done

cat "$tmp"
rm $tmp
