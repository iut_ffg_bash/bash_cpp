#! /bin/bash
#Ce script est chargé de trouvé si le fichier passé en paramètres est connu dans la base et si oui vérifier s'il n'a pas été modifié

if [ "$#" -eq "2" ] #test s'il y a un ou deux arguments
then
	dir="$1/$2" #Dir va prendre la concaténation des deux option
else
	dir="$1" #ou va prendre qu'une seul option
fi

if [ `cat fichierC | grep -c "$dir"` -ne "0" ] #Si au moins une ligne correspond au fichier demandé
then

	md5=`cat fichierC | grep "$dir" | cut -d ":" -f3` #On prend le md5 qui est indexé
	newMd5=`md5sum "$dir"` #On fait un nouveau md5

	if [ $md5 -nq $newMd5 ] #On compare les deux md5
	then #Si les md5 sont différents
		./suppWord.sh "$1" "$2" #On supprime les infos concernant ce fichier dans la base
		./index $dir #On lance une nouvelle indexation
	fi
else
	./newIndex $dir #On lance une indexation sur ce dernier si le fichier demandé n'est pas trouvé dans le fichierC
fi
