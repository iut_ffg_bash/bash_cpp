#!/bin/bash
# Données : Prend en paramètre 'x' mots à rechercher dans la base de donées
# Résultats : On va chercher les informations (des mots données en paramètre) dans la base de données et on les affiches sur la console 

nbWord=$# # On stocke le nombre de paramètres envoyé à notre fonction

if [[ ! -e "$HOME/.indexAndSearch" ]]; then # Si notre base de données n'existe pas :
	mkdir $HOME/.indexAndSearch # on la créer
	cp ../html/template.html $HOME/.indexAndSearch/
	cp ../html/bootstrap $HOME/.indexAndSearch/
else
	if [[ ! -e "$HOME/.indexAndSearch/template.html" ]]; then
		cp ../html/template.html $HOME/.indexAndSearch/
	fi
	if [[ ! -e "$HOME/.indexAndSearch/boostrap" ]]; then
		cp -R ../html/bootstrap $HOME/.indexAndSearch/
	fi
fi

template="$HOME/.indexAndSearch/template.html"
if [[ -e "$HOME/.indexAndSearch/index.html" ]]; then
		rm $HOME/.indexAndSearch/index.html
fi
touch $HOME/.indexAndSearch/index.html

if [[ -e "$HOME/.indexAndSearch/index.html" ]]; then
	echo "Yes"
else
	echo "No"
fi

html="$HOME/.indexAndSearch/index.html"

tmphtml=`mktemp tmphtml.XXXXXX`
cat "$template" > $tmphtml

if [[ $nbWord -ge 1 ]]; then # Si il y a au moins un argument, si on a au moins un mot à rechercher :
	echo "Loading ..."
	sed "s/\$Globals/<p class=\"card-text\"><\/br>\$Globals/1" $tmphtml > $html
	cat $html > $tmphtml
	for word in "$@"; do # Pour tous les mots à rechercher :
		fichierB="$HOME/.indexAndSearch/$word.txt" # On accède à notre fichier <word>.txt dans notre base de données

		if [[ -e $fichierB ]]; then # Si il existe un fichier "<word>.txt" dans la base de donées alors :
			lineCount=1 # variable compteur qui va compter nos tours de boucle dans la while suivante et qui représente la ligne ou on se trouve dans le fichier
			occTot=0 # Représente le nombre d'occurence total du mot

			while read line; do # Pour chasue ligne du fichier <word>.txt dans la base de données

				if [[ $lineCount -ge 2 ]]; then # Si on se situe au minimum à la ligne '2' :

					if [[ $(($lineCount%2)) -eq 1 ]]; then # Si la ligne est impaire alors elle correspond à des infos sur les occurences

						occ=`echo "$line" | cut -d ':' -f 1` # Donc on isole juste le nombre d'occurence du mot dans un fichier
						occTot=$(($occTot+$occ)) # Que l'on ajoute au occurence totales
					fi
				fi
				lineCount=$(($lineCount+1)) # On passe à la ligne suivante
			done < $fichierB # Fin de la boucle while

			sed "s/\$Globals/<strong>$word<\/strong> : $occTot  found<br\/>\n\$Globals/1" $tmphtml > $html
			cat $html > $tmphtml
		else # Si le fichier <word>.txt n'existe pas, alors il n'est pas dans notre base de données
			sed "s/\$Globals/<strong>$word<\/strong> : 0 found<br\/>\n\$Globals/1" $tmphtml > $html
			cat $html > $tmphtml
		fi
	done
	sed "s/\$Globals/<\/br><\/p>\$Globals/1" $tmphtml > $html
	cat $html > $tmphtml

	sed "s/\$Globals//g" $tmphtml > $html #On supprime tout les $globals
	cat $html > $tmphtml

	for word in "$@"; do # Pour chaque mot recherchés :
		fichierB="$HOME/.indexAndSearch/$word.txt" # On accède à notre fichier <word>.txt dans notre base de données
		sed "s:\$Results:<h2><span class=\"badge badge-dark\">$word<\/span><\/h2><div class=\"card\">\n\$Results:1" $tmphtml > $html
		cat $html > $tmphtml

		if [[ -e $fichierB ]]; then # Si le fichier <word>.txt existe alors il est dans la base de données
			
			lineCount=1 # On instentie notre compteur de ligne à 0

			while read line # Puis on lit le fichier <word>.txt 
			do
				if [[ $lineCount -ge 2 ]]; then # Si on se situe au minimum à la ligne '2' :
					if [[ $(($lineCount%2)) -eq 0 ]]; then # Si la ligne est paire on à des infos sur le fichier 
						rep=`echo "$line" | cut -d ':' -f 1 | sed 's/^|//'` # le repertoire du fichier est le première élement
						fichier=`echo "$line" | cut -d ':' -f 2` # Le nom du fichier est le deuxième élement

						if [[ $lineCount -ge 3 ]]; then
							sed "s:\$Results:<hr><h5 class=\"card-title\">$fichier<\/h5>\n<h6 class=\"card-subtitle mb-2 text-muted\">$rep/$fichier<\/h6>\n\$Results:1" $tmphtml > $html
							cat $html > $tmphtml
						else
							sed "s:\$Results:<h5 class=\"card-title\"><br\/>$fichier<\/h5>\n<h6 class=\"card-subtitle mb-2 text-muted\">$rep/$fichier<\/h6>\n\$Results:1" $tmphtml > $html
							cat $html > $tmphtml
						fi
						
					else  # Si la ligne est impaire on à des infos sur les occurences
						occ=`echo "$line" | cut -d ':' -f 1` # Le nombre d'occurence du fichier en cours de lecture
						sed "s:\$Results:<h5 class=\"card-title\">$occ<\/h5>\n<p class=\"card-text\">[ \$Results:1" $tmphtml > $html
						cat $html > $tmphtml

						# <p class="card-text"> </p>
						for (( i = 2; i <= $(($occ+1)); i++ )); do # Pour chaque occurence dans le fichier en cours de lecture on va afficher la ligne où il apparait
							if [[ i -eq $(($occ+1)) ]]; then
								content="$(echo -n $line | cut -d ':' -f "$i")" # On ajoute les lignes au fichier content
							else
								content="$(echo -n $line | cut -d ':' -f "$i") - " # On ajoute les lignes au fichier content
							fi
							
							sed "s:\$Results:$content\$Results:1" $tmphtml > $html
							cat $html > $tmphtml
							if [[ $(($(($i-1))%20)) -eq 0 ]]; then # Si il y a plus de 10 occurences on saute une ligne
								sed "s:\$Results:<br\/>\n\$Results:1" $tmphtml > $html
								cat $html > $tmphtml
							fi
						done # Fin de la lecture des occurences
						sed "s:\$Results:]<\/p><br\/>\n\$Results:1" $tmphtml > $html
						cat $html > $tmphtml
					fi
				fi
				lineCount=$(($lineCount+1)) # On saute une ligne 
			done < $fichierB
		else # Si le mot n'existe pas dans notre base de données alors on dit qu'on a rien trouvé :
			sed "s:\$Results:<h5 class=\"card-title\"><\/br>Not found<\/h5>\n<h6 class=\"card-subtitle mb-2 text-muted\">None<\/h6><h5 class=\"card-title\">0<\/h5>\n<p class=\"card-text\">\[\&emsp;]</p><\/br>\$Results:1" $tmphtml > $html
			cat $html > $tmphtml
		fi
		sed "s/\$Results/<\/div><br\/>\n\$Results/1" $tmphtml > $html
		cat $html > $tmphtml

	done

	sed "s/\$Results//g" $tmphtml > $html #On supprime tout les $globals
	cat $html > $tmphtml
	echo "End."
	nohup sensible-browser $html > /dev/null 2>&1&
else # Sinon si il ya moins d'un argument on affiche ce message d'erreur :
	echo "Error : The function \"$0\" must takes more than 0 arguments."
fi

#rm "$log" # On supprime notre fichier temporaire 

rm "$tmphtml"