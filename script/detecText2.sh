#! /bin/bash

if [ -e "$1" ]
then

	case $# in
		0)
		;;
		1)
		racine="$1"
		;;
		*)
		echo "erreur de syntaxe $0 <fichier où l'on veut chercher>"
		exit
		;;
	esac
	for dir in `ls -R $racine | sed 's/:/\//g'`
	do

		if test -d "$dir"
		then
		dossier="$dir"
		fi

		if [ `file "$dossier$dir" | cut -d ":" -f2 | grep -c " text"` -eq "1" ] #Regarde si le fichier pointer est un text
		then
			echo "$dossier$dir"
			./mdCheck.sh $dossier $dir
		fi

	done

else
echo "$1 isn't directory try --help for more information"
fi
