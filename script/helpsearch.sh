#!/bin/bash
echo -e "-*- \033[4mindexAndSearch > HELP > SEARCH\033[m -*- \n"

# Pattern
echo "PATTERN"
echo -e "    \$ idnsc --search [\033[4mOPTIONS\033[m] \033[4mword\033[m\n"
echo -e "    \$ idnsc -s [\033[4mOPTIONS\033[m] \033[4mword\033[m \033[4mword2\033[m\n"

# Description
echo -e "DESCRIPTION"
echo -e "    This function is used to store, in a database, all the words in all the text-files of a folder and his subfolders."
echo -e "    After this, you will be able to use the search function (more informations with : \$ idnsc --help search) to search a word in the database.\n"

# List of options + descriptions
echo "OPTIONS"
echo -e "    -x, --html\n\tDisplay the result of the search as an html page."
echo -e "\t    For exemple : "
echo -e "\t      \$ idnsc --search hello --html"