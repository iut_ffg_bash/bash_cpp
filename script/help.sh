#!/bin/bash
echo -e "-*- \033[4mindexAndSearch > HELP\033[m -*-"

# Pattern
echo -e "\nPATTERN"
echo -e "    Warning : You must use the \"index\" function before use the \"search\" function.\n"
echo -e "\t\$ idnsc \033[4mACTION\033[m  [\033[4mOPTIONS\033[m]\n"

# Description
echo "DESCRIPTION"
echo -e "    This function is used to search, in all the text-files of a specific directory, the place and the number of the occurrences of a specific word.\n"

# Action
echo "ACTIONS"
echo -e "    -i, --index\n\tSave all words of all the text files of the directory in a database."
echo -e "\t    For exemple : "
echo -e "\t      \$ idnsc --index /home/zdupont\n"

echo -e "    -s, --search\n\tSearch in the current database all the same words as the one entered in paramater."
echo -e "\tIt returns, for each file that the word appear, the number of occurrences of the word in the file and for each occurrence the corresponding line."
echo -e "\t    For exemple : "
echo -e "\t      \$ idnsc --search hello\n"

echo -e "    -c, --clean\n\tDelete the current database."
echo -e "\t    For exemple : "
echo -e "\t      \$ idnsc --clean\n"

# List of options
echo "OPTIONS"
echo -e "    -h, --help\n\tTo get more informations about \"-i\"(\"--index\") or \"-s\"(\"--search\")."
echo -e "\t    For exemple : "
echo -e "\t      \$ idnsc --help index"
echo -e "\t      \$ idnsc --help search\n"

