#! /bin/bash
#Ce script doit rechercher si des fichiers indexé ont été supprimé et faire appel a suppWord s'il en trouve

home="$HOME/.indexAndSearch"

while read line
do
	if [[ ! "$line" =~ ^\#.* ]]
	then
		dir=`echo "$line" | cut -d ":" -f1`
		fic=`echo "$line" | cut -d ":" -f2`
		
		if [ ! -e "$dir/$fic" ]
		then
			./suppWord.sh "$dir" "$fic"
		fi
	fi
done < "$home/fichierC.txt"
