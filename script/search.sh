#!/bin/bash
# Données : Prend en paramètre 'x' mots à rechercher dans la base de donées
# Résultats : On va chercher les informations (des mots données en paramètre) dans la base de données et on les affiches sur la console 

nbWord=$# # On stocke le nombre de paramètres envoyé à notre fonction

if [[ ! -e "$HOME/.indexAndSearch" ]]; then # Si notre base de données n'existe pas :
	mkdir $HOME/.indexAndSearch # on la créer
fi

log=$(mktemp $HOME/.indexAndSearch/idnscSearch.XXXXXX) # On créer un fichier temporaire qui va contenir temporairement nos informations à afficher

if [[ $nbWord -ge 1 ]]; then # Si il y a au moins un argument, si on a au moins un mot à rechercher :

	echo -e "##########################################" # Style++
	echo "---------- GLOBAL INFORMATIONS -----------" # On va afficher les informations générales
	for word in "$@"; do # Pour tous les mots à rechercher :
		fichierB="$HOME/.indexAndSearch/$word.txt" # On accède à notre fichier <word>.txt dans notre base de données

		if [[ -e $fichierB ]]; then # Si il existe un fichier "<word>.txt" dans la base de donées alors :
			lineCount=1 # variable compteur qui va compter nos tours de boucle dans la while suivante et qui représente la ligne ou on se trouve dans le fichier
			occTot=0 # Représente le nombre d'occurence total du mot

			while read line; do # Pour chasue ligne du fichier <word>.txt dans la base de données

				if [[ $lineCount -ge 2 ]]; then # Si on se situe au minimum à la ligne '2' :

					if [[ $(($lineCount%2)) -eq 1 ]]; then # Si la ligne est impaire alors elle correspond à des infos sur les occurences

						occ=`echo "$line" | cut -d ':' -f 1` # Donc on isole juste le nombre d'occurence du mot dans un fichier
						occTot=$(($occTot+$occ)) # Que l'on ajoute au occurence totales
					fi
				fi
				lineCount=$(($lineCount+1)) # On passe à la ligne suivante
			done < $fichierB # Fin de la boucle while

			if [[ $occTot -ge 2 ]]; then # Si il y a plus d'une occurence du mot :
				echo -e "  * $occTot words \"\033[4m$word\033[m\" found."
			else # Si il ya une ou moins d'une occurence :
				echo -e "  * $occTot word \"\033[4m$word\033[m\" found."
			fi
		else # Si le fichier <word>.txt n'existe pas, alors il n'est pas dans notre base de données
			echo -e "  * The word \"\033[4m$word\033[m\" was not found."
		fi
	done

	echo -e "---------------- RESULTS -----------------" # Maintenant on va afficher plus d'informations sur les occurences

	for word in "$@"; do # Pour chaque mot recherchés :
		fichierB="$HOME/.indexAndSearch/$word.txt" # On accède à notre fichier <word>.txt dans notre base de données
		if [[ -e $fichierB ]]; then # Si le fichier <word>.txt existe alors il est dans la base de données
			echo -e "  * Results for the word \"\033[4m$word\033[m\" >" # On indique sur quel mot on va donner les infos
			
			lineCount=1 # On instentie notre compteur de ligne à 0

			while read line # Puis on lit le fichier <word>.txt 
			do
				content="      |   |   |    lines : " # Style++
				if [[ $lineCount -ge 2 ]]; then # Si on se situe au minimum à la ligne '2' :
					if [[ $(($lineCount%2)) -eq 0 ]]; then # Si la ligne est paire on à des infos sur le fichier 
						rep=`echo "$line" | cut -d ':' -f 1 | sed 's/^|//'` # le repertoire du fichier est le première élement
						fichier=`echo "$line" | cut -d ':' -f 2` # Le nom du fichier est le deuxième élement
						echo -e "      | Folder : \"$fichier\" > Repository : \"$rep\" >" # Mise en page
					else  # Si la ligne est impaire on à des infos sur les occurences
						occ=`echo "$line" | cut -d ':' -f 1` # Le nombre d'occurence du fichier en cours de lecture
						echo -e "      |   | $occ $word :" # On affiche le nombre d'occurence du fichier en cours de lecture
						for (( i = 2; i <= $(($occ+1)); i++ )); do # Pour chaque occurence dans le fichier en cours de lecture on va afficher la ligne où il apparait
							content="$content$(echo -n $line | cut -d ':' -f "$i"), " # On ajoute les lignes au fichier content
							echo "$content" > $log # on stocke les lignes dans le log
							if [[ $(($(($i-1))%10)) -eq 0 ]]; then # Si il y a plus de 10 occurences on saute une ligne
								content="$content \n      |   |   |\t\t   "
							fi
						done # Fin de la lecture des occurences

						res=`echo -e "$(cat $log)" | sed 's/, $//g'` # Puis on affiche les lignes !
						echo -e "$res"
					fi
				fi
				lineCount=$(($lineCount+1)) # On saute une ligne 
			done < $fichierB
		else # Si le mot n'existe pas dans notre base de données alors on dit qu'on a rien trouvé :
			echo -e "  * Results for the word \"\033[4m$word\033[m\" >"
			echo -e "      | Not found."
		fi
	done

	echo -e "##########################################\n\n" # Style++
else # Sinon si il ya moins d'un argument on affiche ce message d'erreur :
	echo "Error : The function \"$0\" must takes more than 0 arguments."
fi

rm "$log" # On supprime notre fichier temporaire 