action=$1
echo "Voulez vous vraiment $action ? [o/N]"
echo "Rentrer : 'o' -> oui"
echo -e "\t  'n' -> non"
read -p "> " reponse

while [[ !($reponse == 'o' || $reponse == 'n' || $reponse == '' ) ]]; do
	echo "--------------------------------------"
	echo "Erreur : \"$reponse\" est une réponse invalide."
	echo "Vous devez rentrer un seul caractère : "
	echo -e "\t'o' -> Pour comfirmer l'action : $action."
	echo -e "\t'n' -> Pour annuler l'action : $action."
	echo "Voulez vous vraiment $action ?"
	read -p ">" reponse
done

if [[ $reponse == 'o' ]]; then
	return 1
else
	return 0
fi