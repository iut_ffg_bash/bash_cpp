#!/bin/bash

tmp=mktemp

if [ -e "$1" ]
then

	case $# in
		0)
		;;
		1)
		racine="$1"
		;;
		*)
		echo "erreur de syntaxe $0 <fichier où l'on veut chercher>"
		exit
		;;
	esac
	
	ls -R $racine | sed 's/:/\//g' > $tmp
	
	while read line
	do
		if test -d "$line"
		then
			dir="$line"
		elif [ `file "$dir$line" | cut -d ":" -f2 | sed 's/\ /\\\ /g' | grep -c " text"` -eq "1" ] #Regarde si le fichier pointer est un text
		then
			echo "$dir$line"
			./mdCheck.sh "$dir" "$line"
		fi
		
	done < $tmp
	
	rm $tmp

else
echo "$1 isn't directory try --help for more information"
fi
